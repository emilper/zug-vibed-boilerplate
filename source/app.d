import vibe.vibe;
import vibe.http.log;

static import std.file;

import mustache;
alias Mustache = MustacheEngine!(string);

void main()
{
	import std.regex;
	import std.functional: toDelegate;

	auto settings = new HTTPServerSettings;
	settings.port = 9090;
	settings.bindAddresses = ["127.0.0.1"];
	settings.accessLogToConsole = true;

	auto router = new URLRouter;

	immutable string[string] pages_list = [
		"/some": "index",
		"/about": "about",
		"/work": "work",
	];

	static foreach(url, file; pages_list) {
		router.get(url, &serve_page!file);

        if ( !match( url, regex("/$") ) ) {
            router.get(url~"/", &serve_page!file);
        }
    }

	auto fs_settings = new HTTPFileServerSettings;
	fs_settings.preWriteCallback = toDelegate(&handleMIME);
	router.get("/css/*", serveStaticFiles("./web/static/public/", fs_settings));
	router.get("/img/*", serveStaticFiles("./web/static/public/", fs_settings));
	router.get("/js/*", serveStaticFiles("./web/static/public/", fs_settings));
	router.get("/*", serveStaticFiles("./web/static/html/", fs_settings));
	// router.get("/*", &not_found);

	listenHTTP(settings, router);
	runApplication();
}


@safe void handleMIME(scope HTTPServerRequest req, scope HTTPServerResponse res, ref string physicalPath) {
	import std.stdio: writeln;

    if (physicalPath.endsWith(".js")) {
        res.contentType = "application/javascript";
    } else if (physicalPath.endsWith(".css")) {
        res.contentType = "text/css";
    } else if (physicalPath.endsWith(".html")) {
        res.contentType = "text/html";
    }
}

void serve_page(string page_file)(HTTPServerRequest req, HTTPServerResponse res) 
{
    Mustache mustache;
    auto context = new Mustache.Context;
	
	string template_folder = "templates/pages/";
	string template_extension = ".mustache";

	if (!std.file.exists(template_folder ~ page_file ~ template_extension)) {
		not_found(req, res);
		return;
	}

    string page_contents = mustache.render("templates/pages/" ~ page_file, context);

    res.headers["Content-type"] = "text/html";
    res.writeBody(render_page(page_contents));
}

string render_page(string contents)
{

    Mustache mustache;
    auto context = new Mustache.Context;
    context["contents"] = contents;

    return mustache.render("templates/main", context);
}

bool check_password(string user, string password)
{
    return user == "yourid" && password =="secret";
}

void hello(HTTPServerRequest req, HTTPServerResponse res)
{
	res.writeBody("Hello, World!");
}

void not_found(HTTPServerRequest req, HTTPServerResponse res)
{
	res.statusCode = 404;
	res.writeBody("Page not found " ~ req.toString);
}

void not_known(HTTPServerRequest req, HTTPServerResponse res)
{
	res.statusCode = 404;
	res.writeBody("Page not known " ~ req.toString);
}
